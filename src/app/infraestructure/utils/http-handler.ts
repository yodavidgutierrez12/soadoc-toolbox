import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {select, Store} from '@ngrx/store';
import {State} from '../redux_store/reducers';
import {map, switchMap, take} from 'rxjs/internal/operators';

@Injectable()
export class HttpHandler {

    private token$: Observable<string>;

    constructor(private _http: HttpClient, private _store: Store<State>) {
        this.token$ = _store.pipe(select(s => s.auth.token));
    }

    requestHelper(url: string , method: string, options?: any): Observable<HttpResponse<any>> {

        return this.token$.pipe(take(1), switchMap(token => {
            // console.log('Calling protected URL ...', token);

            options = options || {responseType: 'json', observe: 'events'};
            options.headers = new HttpHeaders();
            if (token !== null) {

                options.headers.append('Content-Type', 'application/json');
                options.headers.append('Authorization', 'Bearer ' + token);
            } else {
                options.headers.append('Content-Type', 'application/json');
            }
            if (options.body && typeof options.body !== 'string') {
                options.body = JSON.stringify(options.body);
            }

              const   request$ = this._http.request(method, url, options);

            return this.handleResponse(request$, token);
        }));

    }

    uploadHelper(url: string , options?: any): Observable<HttpResponse<any>> {
        return this.token$.pipe(take(1), switchMap(token => {
            options = options || {responseType: 'blob', observe: 'response'};
            options.headers = new HttpHeaders();
            if (token !== null) {
                // options.headers.append('Content-Type', 'multipart/form-data');
                options.headers.append('Accept', 'application/json');
                options.headers.append('Authorization', 'Bearer ' + token);
            } else {
                options.headers.append('Accept', 'application/json');
                // options.headers.append('Content-Type', 'multipart/form-data');
            }

            const request$ = this._http.request(url, 'post', options);
            return this.handleResponse(request$, token);
        }));
    }

    handleResponse(request$: Observable<HttpResponse<any>|ArrayBuffer>, token): Observable<HttpResponse<any>> {

        return request$.pipe(map((res: HttpResponse<any>) => {

            if ('application/json' === res.headers.get('Content-Type')) {
                return res.body;
            }
            return res;
        }));
    }

    public get(url: string, params: any, options?: any): Observable<HttpResponse<any>> {

        options = options || {observe: 'response', responseType : 'json'};

        options.params = params;

        return this.requestHelper( url, 'get', options);
    }

    public post(url: string, body: any, options?: any): Observable<HttpResponse<any>> {

        options = options || {observe: 'response', responseType : 'json'};

        options.body = body;

        return this.requestHelper(url, 'post', options);
    }

    public put(url: string, body: any, options ?: any): Observable<HttpResponse<any>> {

        options = options || {observe: 'response', responseType : 'json'};

        options.body = body;

        return this.requestHelper(url, 'put', options);
    }

    public delete(url: string, options ?: any): Observable<HttpResponse<any>> {
        return this.requestHelper(url, 'delete', options);
    }

    public patch(url: string, body: any, options?: any): Observable<HttpResponse<any>> {

        options = options || {observe: 'response', responseType : 'json'};

        options.body = body;

        return this.requestHelper(url, 'patch', options);
    }

    public putFile(url: string, body: any, options ?: any): Observable<HttpResponse<any>> {

        options = options || {observe: 'response', responseType : 'json'};

        options.body = body;

        return this.uploadHelper(url, options);
    }



}
