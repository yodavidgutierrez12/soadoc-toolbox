import { PAGES_ROUTES_MODULE } from './pages-routing.module';
import { PRIMENG_MODULES } from './../../shared/primeNg/primeng-elements';
import { PagesComponent } from './pages.component';
import { LayoutModule } from './layouts/layout.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './pages/auth/login/login.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

@NgModule({
  declarations: [
      PagesComponent,
      LoginComponent,
      RegisterComponent,
      PageNotFoundComponent
  ],
  exports: [
      PagesComponent
  ],
  imports: [
    CommonModule,
    LayoutModule,
    ...PRIMENG_MODULES,
    PAGES_ROUTES_MODULE
  ]
})
export class PagesModule { }
