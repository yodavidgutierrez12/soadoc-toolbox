import { LoadingService } from './infraestructure/services/loading/loading.service';

import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { APP_ROUTES_MODULE} from './app.routes';

//PrimeNG Modules
import {PRIMENG_MODULES} from './shared/primeNg/primeng-elements';

//Utils
import { HttpHandler } from './infraestructure/utils/http-handler';

//Componenst
import {AppComponent} from './app.component';
import {DashboardDemoComponent} from './demo/view/dashboarddemo.component';
import {SampleDemoComponent} from './demo/view/sampledemo.component';
import {FormsDemoComponent} from './demo/view/formsdemo.component';
import {DataDemoComponent} from './demo/view/datademo.component';
import {PanelsDemoComponent} from './demo/view/panelsdemo.component';
import {OverlaysDemoComponent} from './demo/view/overlaysdemo.component';
import {MenusDemoComponent} from './demo/view/menusdemo.component';
import {MessagesDemoComponent} from './demo/view/messagesdemo.component';
import {MiscDemoComponent} from './demo/view/miscdemo.component';
import {EmptyDemoComponent} from './demo/view/emptydemo.component';
import {ChartsDemoComponent} from './demo/view/chartsdemo.component';
import {FileDemoComponent} from './demo/view/filedemo.component';
import {UtilsDemoComponent} from './demo/view/utilsdemo.component';
import {DocumentationComponent} from './demo/view/documentation.component';

//Services
import { ServiceModule } from './infraestructure/services/service.module';

//NGRX
import {StoreModule} from '@ngrx/store';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import { reducers, metaReducers } from './infraestructure/redux_store/reducers';
import { environment } from 'src/environments/environment';
import { PagesModule } from './ui/components/pages.module';


@NgModule({
    declarations: [
        AppComponent,
        DashboardDemoComponent,
        SampleDemoComponent,
        FormsDemoComponent,
        DataDemoComponent,
        PanelsDemoComponent,
        OverlaysDemoComponent,
        MenusDemoComponent,
        MessagesDemoComponent,
        MiscDemoComponent,
        ChartsDemoComponent,
        EmptyDemoComponent,
        FileDemoComponent,
        UtilsDemoComponent,
        DocumentationComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ServiceModule,
        HttpClientModule,
        PagesModule,
        BrowserAnimationsModule,
        ...PRIMENG_MODULES,
        StoreModule.forRoot(reducers, {metaReducers}),
        APP_ROUTES_MODULE,
        StoreRouterConnectingModule,
        !environment.production
            ? StoreDevtoolsModule.instrument({ maxAge: 50 }): []
    ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy}
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
