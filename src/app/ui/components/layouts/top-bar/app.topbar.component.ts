import { PagesComponent } from './../../pages.component';
import {Component} from '@angular/core';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'

})
export class AppTopbarComponent {

    constructor(public app: PagesComponent) {}

}
