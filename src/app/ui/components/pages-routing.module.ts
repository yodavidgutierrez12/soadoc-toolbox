import { MiscDemoComponent } from './../../demo/view/miscdemo.component';
import { EmptyDemoComponent } from './../../demo/view/emptydemo.component';
import { ChartsDemoComponent } from './../../demo/view/chartsdemo.component';
import { FileDemoComponent } from './../../demo/view/filedemo.component';
import { UtilsDemoComponent } from './../../demo/view/utilsdemo.component';
import { DocumentationComponent } from './../../demo/view/documentation.component';
import { MessagesDemoComponent } from './../../demo/view/messagesdemo.component';
import { MenusDemoComponent } from './../../demo/view/menusdemo.component';
import { OverlaysDemoComponent } from './../../demo/view/overlaysdemo.component';
import { PanelsDemoComponent } from './../../demo/view/panelsdemo.component';
import { DataDemoComponent } from './../../demo/view/datademo.component';
import { FormsDemoComponent } from './../../demo/view/formsdemo.component';
import { SampleDemoComponent } from './../../demo/view/sampledemo.component';
import { DashboardDemoComponent } from './../../demo/view/dashboarddemo.component';
import { PagesComponent } from './pages.component';
import { Routes, RouterModule } from '@angular/router';


//Array of routes
const PAGES_ROUTES: Routes = [
  {path: '',
   component: PagesComponent,
   children: [
    {path: 'dashboard', component: DashboardDemoComponent},
    {path: 'sample', component: SampleDemoComponent},
    {path: 'forms', component: FormsDemoComponent},
    {path: 'data', component: DataDemoComponent},
    {path: 'panels', component: PanelsDemoComponent},
    {path: 'overlays', component: OverlaysDemoComponent},
    {path: 'menus', component: MenusDemoComponent},
    {path: 'messages', component: MessagesDemoComponent},
    {path: 'misc', component: MiscDemoComponent},
    {path: 'empty', component: EmptyDemoComponent},
    {path: 'charts', component: ChartsDemoComponent},
    {path: 'file', component: FileDemoComponent},
    {path: 'utils', component: UtilsDemoComponent},
    {path: 'documentation', component: DocumentationComponent},
     {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
   ]}
];

export const PAGES_ROUTES_MODULE = RouterModule.forChild(PAGES_ROUTES);
